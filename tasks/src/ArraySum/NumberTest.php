<?php

namespace App\Tasks\ArraySum;


use PHPUnit\Framework\TestCase;

class NumberTest extends TestCase
{
    public static function testIsGreatThanFive(): void
    {
        self::assertTrue(Number::isGreatThanFive(6));
        self::assertFalse(Number::isGreatThanFive(1));
    }

    public static function testIsValidNumber(): void
    {
        $allowed = [0, 3, 5];
        $valid = [30, 350, 5, 53, 305, 503, 530, 3000, 53035];
        $invalid = [37, 1, 5307, 339];

        foreach ($valid as $validNumber) {
            self::assertTrue(Number::isValidNumber($validNumber, $allowed));
        }

        foreach ($invalid as $invalidNumber) {
            self::assertFalse(Number::isValidNumber($invalidNumber, $allowed));
        }
    }
}
