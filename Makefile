init: init-ci
init-ci: docker-down-clear tasks-clear \
	docker-pull docker-build docker-up \
	tasks-init

up: docker-up
down: docker-down
restart: down up

test: tasks-tests

###########################################################
############### DOCKER ####################################
###########################################################
docker-start:
	sudo systemctl start docker

docker-pull:
	docker-compose pull

docker-build:
	docker-compose build --pull

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down --remove-orphans

docker-down-clear:
	docker-compose down -v --remove-orphans


###########################################################
############### TASKS #####################################
###########################################################
tasks-composer-init:
	docker-compose run --rm tasks composer init

tasks-composer-install:
	docker-compose run --rm tasks composer install

tasks-install-php-unit:
	#docker-compose run --rm tasks composer require --dev composer require phpunit/phpunit
	docker-compose run --rm tasks php vendor/bin/phpunit --generate-configuration

#tasks-install-php-cs-fixer:
#	docker-compose run --rm tasks mkdir --parents tools/php-cs-fixer
#	docker-compose run --rm tasks composer require --working-dir=tools/php-cs-fixer friendsofphp/php-cs-fixer

#tasks-install-psalm:
#	docker-compose run --rm tasks composer require --dev vimeo/psalm
#	docker-compose run --rm tasks composer require --dev psalm/plugin-phpunit
#	docker-compose run --rm tasks ./vendor/bin/psalm --init
#	docker-compose run --rm tasks vendor/bin/psalm-plugin enable psalm/plugin-phpunit

tasks-clear:
	docker-compose run --rm tasks sh -c 'rm -rf var/cache/* var/log/* var/test/*'

tasks-init: tasks-composer-install

tasks-tests:
	docker-compose run --rm tasks composer test -- --testsuite=unit

tasks-lint:
	docker-compose run --rm tasks composer php-cs-fixer fix -- --dry-run --diff

tasks-cs-fix:
	docker-compose run --rm tasks composer php-cs-fixer fix

tasks-psalm:
	docker-compose run --rm tasks composer psalm -- --no-diff


###########################################################
############### FRONTEND ##################################
###########################################################
frontend-create-react-app:
	docker-compose run --rm frontend-node-cli yarn create react-app my-app --template typescript

frontend-yarn-install:
	docker-compose run --rm frontend-node-cli yarn install

frontend-yarn-upgrade:
	docker-compose run --rm frontend-node-cli yarn upgrade

frontend-init: frontend-yarn-install

frontend-clear:
	docker-compose run --rm frontend-node sh -c 'rm -rf .ready build'

frontend-ready:
	docker-compose run --rm frontend-node sh -c 'touch .ready'














#tasks-clear:
#	docker run --rm -v ${PWD}/api:/app -w /app alpine sh -c 'rm -rf var/cache/* var/log/* var/test/*'
#
#tasks-init: tasks-permissions tasks-composer-install tasks-wait-db tasks-migrations tasks-fixtures

tasks-permissions:
	docker run --rm -v ${PWD}/api:/app -w /app alpine chmod 777 var/cache var/log var/test

tasks-composer-update:
	docker-compose run --rm tasks composer update

tasks-wait-db:
	docker-compose run --rm tasks wait-for-it tasks-postgres:5432 -t 30

tasks-migrations:
	docker-compose run --rm tasks composer app migrations:migrate -- --no-interaction

tasks-fixtures:
	docker-compose run --rm tasks composer app fixtures:load

tasks-backup:
	docker-compose run --rm tasks-postgres-backup

tasks-check: tasks-validate-schema tasks-lint tasks-analyze tasks-test

tasks-validate-schema:
	docker-compose run --rm tasks composer app orm:validate-schema

#tasks-lint:
#	docker-compose run --rm tasks composer lint
#	docker-compose run --rm tasks composer php-cs-fixer fix -- --dry-run --diff

#tasks-cs-fix:
#	docker-compose run --rm tasks composer php-cs-fixer fix

tasks-analyze:
	docker-compose run --rm tasks composer psalm -- --no-diff

tasks-analyze-diff:
	docker-compose run --rm tasks composer psalm

tasks-test:
	docker-compose run --rm tasks composer test

tasks-test-coverage:
	docker-compose run --rm tasks composer test-coverage

tasks-test-unit:
	docker-compose run --rm tasks composer test -- --testsuite=unit

tasks-test-unit-coverage:
	docker-compose run --rm tasks composer test-coverage -- --testsuite=unit

tasks-test-functional:
	docker-compose run --rm tasks composer test -- --testsuite=functional

tasks-test-functional-coverage:
	docker-compose run --rm tasks composer test-coverage -- --testsuite=functional
